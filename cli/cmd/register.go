package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var (
	url      string
	username string
	password string

	registerCmd = &cobra.Command{
		Use:   "register",
		Short: "Register a new user",
		Long:  `Register a new user`,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("Creating user", username, "on url", url)
			// TODO
		},
	}
)

func init() {
	registerCmd.PersistentFlags().StringVarP(&url, "url", "u", "http://localhost:9090/api", "url of the api")
	registerCmd.PersistentFlags().StringVarP(&username, "username", "U", "", "username")
	registerCmd.MarkPersistentFlagRequired("username")
	registerCmd.PersistentFlags().StringVarP(&password, "password", "p", "", "username")
	registerCmd.MarkPersistentFlagRequired("password")
	rootCmd.AddCommand(registerCmd)

}
