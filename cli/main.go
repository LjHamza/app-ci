package main

import (
	"app-cli/cmd"
)

func main() {
	cmd.Execute()
}
